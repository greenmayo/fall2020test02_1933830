/*
This is the main class for the second question
*/
public class PayrollManagement {
    public static void main(String[] args) {
        Employee Jon = new SalariedEmployee(100000);
        Employee Dany = new HourlyEmployee(35, 20);
        Employee Dog = new UnionizedHourlyEmployee(70, 20, 40, 2);
        Employee Bran = new SalariedEmployee(90000);
        Employee Tyrion = new HourlyEmployee(69, 420);
        Employee[] employees = {Jon, Dany, Dog, Bran, Tyrion};
        System.out.println("Total expenses: " + getTotalExpenses(employees));
    }
//    calculating total weekly expenses
    public static double getTotalExpenses(Employee[] employees) {
        double totalExpenses = 0;
        for (Employee e:
             employees) {
            totalExpenses += e.getWeeklyPay();
        }
        return totalExpenses;
    }

}
