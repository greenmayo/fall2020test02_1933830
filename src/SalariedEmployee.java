public class SalariedEmployee implements Employee {
    private double yearlySalary;

    public SalariedEmployee(double yearlySalary){
        this.yearlySalary = yearlySalary;
    }
    public double getYearlySalary() {
        return yearlySalary;
    }

    @Override // yearly salary by the number of weeks
    public double getWeeklyPay() {
        return this.yearlySalary/52;
    }
}
