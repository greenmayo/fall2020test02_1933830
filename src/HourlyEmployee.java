public class HourlyEmployee implements Employee {
    private double hoursWorked;
    private double hourlyPay;

    public HourlyEmployee(double hoursWorked, double hourlyPay) {
        this.hoursWorked = hoursWorked;
        this.hourlyPay = hourlyPay;
    }

    @Override // simply multiplication hour by $/hr
    public double getWeeklyPay() {
        return this.hourlyPay*this.hoursWorked;
    }

    public double getHoursWorked() {
        return hoursWorked;
    }

    public double getHourlyPay() {
        return hourlyPay;
    }
}
