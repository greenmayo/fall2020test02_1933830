public class UnionizedHourlyEmployee extends HourlyEmployee {
    private double maxHoursPerWeek;
    private double overtimeRate;
    public UnionizedHourlyEmployee(double hoursWorked, double hourlyPay, double maxHoursPerWeek, double overtimeRate) {
        super(hoursWorked, hourlyPay);
        this.maxHoursPerWeek = maxHoursPerWeek;
        this.overtimeRate = overtimeRate;
    }

    @Override
//    weekly pay for unionized employee based on how much overtime he/she had
    public double getWeeklyPay() {
        if (getHoursWorked()<=this.maxHoursPerWeek)
            return getHourlyPay()*getHoursWorked();
        else {
            return this.overtimeRate*getHourlyPay()*(getHoursWorked()-this.maxHoursPerWeek) +
                    this.maxHoursPerWeek*getHourlyPay();
        }
    }
}
