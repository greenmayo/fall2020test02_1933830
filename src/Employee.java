public interface Employee {
    // each class will have a getWeeklyPay
    double getWeeklyPay();
}
