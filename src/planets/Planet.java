package planets;

import java.util.Objects;

/**
 * A class to store information about a planet
 * @author Daniel
 *
 */
public class Planet implements Comparable<Planet> {
	/**
	 * The name of the solar system the planet is contained within
	 */
	private String planetarySystemName;
	
	/**
	 * The name of the planet.
	 */
	private String name;
	
	/**
	 * From inside to outside, what order is the planet. (e.g. Mercury = 1, Venus = 2, etc)
	 */
	private int order;

	@Override
	public boolean equals(Object o) { // overriding for use in collection, consistent with hashCode
		if (this == o) return true;
		if (o == null || getClass() != o.getClass()) return false;
		Planet planet = (Planet) o;
		return planetarySystemName.equals(planet.planetarySystemName) &&
				name.equals(planet.name);
	}

	@Override
	public int hashCode() {
		return Objects.hash(planetarySystemName, name); // getting a hashCode based only on names
	}

	@Override
	public int compareTo(Planet o) { // for use in Arrays.sort(), based on name(ascending) and radius after (descending)
		int planetNameDiff = this.getName().compareTo(o.getName()); // ascending by name
		if (planetNameDiff == 0) { // same name, sort by radius
			planetNameDiff = (int) (o.getRadius()-this.radius); // same as if < > statements, descending by radius
			// since radius is a double, we cast that  to int, we just ned the sign anyway
		}

		return planetNameDiff; // should work
	}

	/**
	 * The size of the radius in Kilometers.
	 */
	private double radius;

	/**
	 * A constructor that initializes the planets.Planet object
	 * @param planetarySystemName The name of the system that the planet is a part of
	 * @param name The name of the planet
	 * @param order The order from inside to out of how close to the center the planet is
	 * @param radius The radius of the planet in kilometers
	 */
	public Planet(String planetarySystemName, String name, int order, double radius) {
		this.planetarySystemName = planetarySystemName;
		this.name = name;
		this.order = order;
		this.radius = radius;
	}

	/**
	 * @return The name of the planetary system (e.g. "the solar system")
	 */
	public String getPlanetarySystemName() {
		return planetarySystemName;
	}

	/**
	 * @return The name of the planet (e.g. Earth or Venus)
	 */
	public String getName() {
		return name;
	}

	/**
	 * @return The rank of the planetary system
	 */
	public int getOrder() {
		return order;
	}


	/**
	 * @return The radius of the planet in question.
	 */
	public double getRadius() {
		return radius;
	}
}
