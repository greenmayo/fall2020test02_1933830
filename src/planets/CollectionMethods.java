package planets;

import java.util.ArrayList;
import java.util.Collection;

public class CollectionMethods {
    public static Collection<Planet> getLargerThan(Collection<Planet> planets, double size){
        ArrayList<Planet> bigPlanets = new ArrayList<>();
        for (Planet p:
             planets) {
            if (p.getRadius()>=size) // adds only planets bigger than input size
                bigPlanets.add(p);
        }
        return bigPlanets;
    }
}
